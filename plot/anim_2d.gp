set term postscript eps enhanced color

set grid
set hidden3d
set zrange [0:2.7]

do for [i=1:10] {
  outfile = sprintf('results/example_2d_%03.0f.eps', i)
  set output outfile

  splot 'data/example_2d_'.i.'-h' matrix with lines notitle
}
