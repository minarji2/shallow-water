set style line 1 lc rgb '#595959' pt 5   # square
set style line 2 lc rgb '#a6a6a6' pt 7   # circle
set style line 3 lc rgb 'black' pt 9   # triangle

set xlabel 'Position [m]'

set term postscript eps enhanced color font ',20'
set key center left
set output 'results/example_1.eps'
plot 'data/example_1-h' with points ls 1 title 'Water depth [m]', \
     'data/example_1-psi' with points ls 2 title 'Pollutant concentration', \
     'data/example_1-u' with points ls 3 title 'Particle velocity [m/s]'
