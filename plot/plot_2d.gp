set term postscript eps enhanced color

set title "2D shallow water"

set grid
set hidden3d
set zrange [0:2.7]

outfile = 'results/example_2d.eps'
set output outfile

splot 'data/example_2d-h' matrix with lines
