set term postscript eps enhanced color

do for [i=1:600] {
  outfile = sprintf('results/example_1_%03.0f.eps', i)
  set output outfile

  #set title 'Iteration '.i
  set xlabel 'Position [m]'

  plot 'data/example_1_'.i.'-h' with points ls 1 title 'Water depth [m]', \
       'data/example_1_'.i.'-psi' with points ls 2 title 'Pollutant concentration', \
       'data/example_1_'.i.'-u' with points ls 3 title 'Particle velocity [m/s]'
}
