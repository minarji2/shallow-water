from dataclasses import dataclass
from typing import List

from shallow_water.core import VariablesAug1D


@dataclass
class Variables2D:
    h: float
    u: float
    v: float

    @property
    def h_u(self) -> float:
        return self.h * self.u

    @property
    def h_v(self) -> float:
        return self.h * self.v

    def __add__(self, other: "Variables2D"):
        return Variables2D(
            h=self.h + other.h,
            u=self.u + other.u,
            v=self.v + other.v
        )

    def __sub__(self, other: "Variables2D"):
        return Variables2D(
            h=self.h - other.h,
            u=self.u - other.u,
            v=self.v - other.v
        )

    def __mul__(self, value: float):
        return Variables2D(
            h=value * self.h,
            u=value * self.u,
            v=value * self.v
        )

    def __rmul__(self, value: float):
        return self * value

    def horizontal_aug_1d(self) -> VariablesAug1D:
        return VariablesAug1D(h=self.h, u_h=self.u * self.h, psi=self.v)

    def vertical_aug_1d(self) -> VariablesAug1D:
        return VariablesAug1D(h=self.h, u_h=self.v * self.h, psi=self.u)

    def update_from_horizontal_aug_1d(self, var_aug_1d: VariablesAug1D) -> None:
        self.h = var_aug_1d.h
        self.u = var_aug_1d.u
        self.v = var_aug_1d.psi

    def update_from_vertical_aug_1d(self, var_aug_1d: VariablesAug1D) -> None:
        self.h = var_aug_1d.h
        self.u = var_aug_1d.psi
        self.v = var_aug_1d.u


@dataclass
class Grid2D:
    delta_x: float
    delta_y: float
    u: List[List[Variables2D]]


@dataclass(frozen=True)
class InitialConditions2D:
    grid: Grid2D
