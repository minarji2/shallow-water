from dataclasses import dataclass
from typing import Optional, List

import numpy as np

G = 9.81


@dataclass
class VariablesAug1D:
    h: float
    u_h: float
    psi: float
    a: Optional[float] = None

    @property
    def u(self) -> float:
        return self.u_h / self.h

    @property
    def h_psi(self) -> float:
        return self.h * self.psi

    @property
    def a_from_h(self) -> float:
        return np.sqrt(self.h * G)

    def __add__(self, other: "VariablesAug1D"):
        return VariablesAug1D(
            h=self.h + other.h,
            u_h=self.u_h + other.u_h,
            psi=self.psi + other.psi
        )

    def __sub__(self, other: "VariablesAug1D"):
        return VariablesAug1D(
            h=self.h - other.h,
            u_h=self.u_h - other.u_h,
            psi=self.psi - other.psi
        )

    def __mul__(self, value: float):
        return VariablesAug1D(
            h=value * self.h,
            u_h=value * self.u_h,
            psi=value * self.psi
        )

    def __rmul__(self, value: float):
        return self * value


@dataclass
class Grid:
    delta_x: float
    u: List[VariablesAug1D]


@dataclass(frozen=True)
class InitialConditions:
    grid: Grid


def flux(u: VariablesAug1D) -> VariablesAug1D:
    return VariablesAug1D(
        h=u.u_h,
        u_h=u.h * (u.u ** 2) + 0.5 * G * (u.h ** 2),
        psi=u.h * u.u * u.psi
    )
