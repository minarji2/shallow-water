from loguru import logger

from shallow_water.core import InitialConditions, Grid, VariablesAug1D
from shallow_water.riemann_solver import HLLRiemannSolver
from shallow_water.solver import GodunovSolver, SolverParams, Solver, LaxFriedrichsSolver, LaxWendroffSolver


def init_solver_1() -> Solver:
    u_l = VariablesAug1D(h=1, u_h=2.5, psi=1)
    u_r = VariablesAug1D(h=0.1, u_h=0, psi=0)
    num_pts = 400
    x_max = 50
    x_0 = 10
    delta_x = x_max / (num_pts - 1)
    return LaxWendroffSolver(
        initial_condition=InitialConditions(
            grid=Grid(
                delta_x=delta_x,
                u=[u_l if i * delta_x < x_0 else u_r for i in range(num_pts)]
            )
        ),
        params=SolverParams(
            cfl_number=0.5,
            max_t=7
        )
    )


def init_solver_2() -> Solver:
    u_l = VariablesAug1D(h=1, u_h=-5, psi=1)
    u_r = VariablesAug1D(h=1, u_h=5, psi=0)
    num_pts = 400
    total_x = 50
    delta_x = total_x / (num_pts - 1)
    x_0 = 25

    return LaxFriedrichsSolver(
        initial_condition=InitialConditions(
            grid=Grid(
                delta_x=delta_x,
                u=[u_l if i * delta_x < x_0 else u_r for i in range(num_pts)]
            )
        ),
        params=SolverParams(
            cfl_number=0.1,
            max_t=1.5
        )
    )


def init_solver_3() -> Solver:
    u_in = VariablesAug1D(h=2.5, u_h=0, psi=0)
    u_out = VariablesAug1D(h=.5, u_h=0, psi=0)
    num_pts = 100
    x_max = 40
    x_c, r = 20, 5

    delta_x = x_max / (num_pts - 1)
    return GodunovSolver(
        riemann_solver=HLLRiemannSolver(alternative_h_star=False),
        initial_condition=InitialConditions(
            grid=Grid(
                delta_x=delta_x,
                u=[u_in if (i * delta_x - x_c) ** 2 <= r else u_out for i in range(num_pts)]
            )
        ),
        params=SolverParams(
            cfl_number=0.1,
            max_t=1
        )
    )


def example_1(output_path: str):
    logger.info('Example 1')
    solver = init_solver_1()
    solver.solve(verbose=True)
    solver.save(output_path)


def example_2(output_path: str):
    logger.info('Example 2')
    solver = init_solver_2()
    solver.solve(verbose=True)
    solver.save(output_path)


def example_3(output_path: str):
    logger.info('Example 3')
    solver = init_solver_3()
    solver.solve(verbose=True)
    solver.save(output_path)


if __name__ == '__main__':
    example_1('data/example_1')
