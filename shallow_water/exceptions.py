class ShException(Exception):
    pass


class UnknownSolverMethod(ShException):
    pass
