import abc
import enum
from copy import deepcopy
from dataclasses import dataclass
from typing import Optional

import numpy as np
from loguru import logger

from shallow_water.core import Grid, InitialConditions, flux, VariablesAug1D
from shallow_water.exceptions import UnknownSolverMethod
from shallow_water.riemann_solver import RiemannSolver
from shallow_water.utils import create_csv


@dataclass(frozen=True)
class SolverParams:
    cfl_number: float
    max_t: float


class SolverMethod(enum.Enum):
    Godunov = 'godunov'
    LaxFriedrichs = 'lax_friedrichs'
    LaxWendroff = 'lax_wendroff'
    FORCE = 'force'


class Solver(abc.ABC):

    def __init__(self, initial_condition: InitialConditions, params: SolverParams):
        self._grid = initial_condition.grid
        self._params = params
        self._time = 0

    @abc.abstractmethod
    def f_ip(self, i: int, delta_t: float) -> VariablesAug1D:
        pass

    @property
    @abc.abstractmethod
    def method(self) -> SolverMethod:
        pass

    def get_delta_t(self) -> float:
        s_max = np.max([np.abs(u.u) + u.a_from_h for u in self._grid.u])
        return self._params.cfl_number * self._grid.delta_x / s_max

    def _iteration(self, delta_t: float):
        new_grid = deepcopy(self._grid)
        coef = delta_t / self._grid.delta_x
        for i in range(1, len(self._grid.u) - 1):
            new_grid.u[i] = self._grid.u[i] - coef * (self.f_ip(i, delta_t) - self.f_ip(i - 1, delta_t))
        self._grid = new_grid

    def solve(self, file_path: Optional[str] = None, verbose: bool = False) -> Grid:
        iteration = 0
        while self._time < self._params.max_t:
            delta_t = min(self.get_delta_t(), self._params.max_t - self._time)
            self._iteration(delta_t)
            self._time += delta_t
            iteration += 1
            if verbose:
                logger.info('{}:\tt={}', iteration, self._time)
            if file_path is not None:
                self.save(file_path + '_' + str(iteration))
        return self._grid

    def save(self, path: str):
        data_h = [[self._grid.delta_x * i, u.h] for i, u in enumerate(self._grid.u)]
        data_u = [[self._grid.delta_x * i, u.u] for i, u in enumerate(self._grid.u)]
        data_psi = [[self._grid.delta_x * i, u.psi] for i, u in enumerate(self._grid.u)]
        create_csv(path + '-h', data_h)
        create_csv(path + '-u', data_u)
        create_csv(path + '-psi', data_psi)


class GodunovSolver(Solver):

    def __init__(self, riemann_solver: RiemannSolver,
                 initial_condition: InitialConditions,
                 params: SolverParams):
        super().__init__(initial_condition=initial_condition, params=params)
        self._riemann_solver = riemann_solver

    @property
    def method(self) -> SolverMethod:
        return SolverMethod.Godunov

    def f_ip(self, i: int, delta_t: float) -> VariablesAug1D:
        u_l, u_r = self._grid.u[i], self._grid.u[i + 1]
        return self._riemann_solver.get_flux(u_l, u_r)


class LaxFriedrichsSolver(Solver):

    @property
    def method(self) -> SolverMethod:
        return SolverMethod.LaxFriedrichs

    def f_ip(self, i: int, delta_t: float) -> VariablesAug1D:
        u_i, u_ip = self._grid.u[i], self._grid.u[i + 1]
        f_i, f_ip = flux(u_i), flux(u_ip)
        return 0.5 * (f_i + f_ip) + 0.5 * self._grid.delta_x / delta_t * (u_i - u_ip)


class LaxWendroffSolver(Solver):

    @property
    def method(self) -> SolverMethod:
        return SolverMethod.LaxWendroff

    def f_ip(self, i: int, delta_t: float) -> VariablesAug1D:
        u_i, u_ip = self._grid.u[i], self._grid.u[i + 1]
        f_i, f_ip = flux(u_i), flux(u_ip)
        u_lw_ip = 0.5 * (u_i + u_ip) + 0.5 * delta_t / self._grid.delta_x * (f_i - f_ip)
        return flux(u_lw_ip)


class FORCESolver(Solver):

    @property
    def method(self) -> SolverMethod:
        return SolverMethod.FORCE

    def f_ip(self, i: int, delta_t: float) -> VariablesAug1D:
        f_ip_lf = LaxFriedrichsSolver.f_ip(self, i, delta_t)  # noqa
        f_ip_lw = LaxWendroffSolver.f_ip(self, i, delta_t)  # noqa
        return 0.5 * (f_ip_lf + f_ip_lw)


class SolverFactory:

    def __init__(self, method: SolverMethod):
        self._method = method

    def _get_solver_class(self):
        if self._method == SolverMethod.Godunov:
            return GodunovSolver
        if self._method == SolverMethod.LaxFriedrichs:
            return LaxFriedrichsSolver
        if self._method == SolverMethod.LaxWendroff:
            return LaxWendroffSolver
        if self._method == SolverMethod.FORCE:
            return FORCESolver
        raise UnknownSolverMethod(f'Unknown solver method {self._method}.')

    def create(self, initial_condition: InitialConditions,
               params: SolverParams,
               **kwargs) -> Solver:
        solver_class = self._get_solver_class()
        return solver_class(
            initial_condition=initial_condition,
            params=params,
            **kwargs
        )
