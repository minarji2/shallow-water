from copy import deepcopy

from shallow_water.core_2d import InitialConditions2D, Grid2D, Variables2D
from shallow_water.solver import SolverFactory, SolverMethod
from shallow_water.solver_2d import Solver2D, SolverParams2D


def init_solver() -> Solver2D:
    num_pts_x, num_pts_y = 100, 100

    # Parameters from page 245
    x_c, y_c, r = 20, 20, 5  # 2.5
    h_ins, h_out = 2.5, .5
    total_x, total_y = 40, 40
    var_ins = Variables2D(h=h_ins, u=0, v=0)
    var_out = Variables2D(h=h_out, u=0, v=0)

    def u(x: float, y: float) -> Variables2D:
        x -= x_c
        y -= y_c
        if x ** 2 + y ** 2 <= r ** 2:
            return deepcopy(var_ins)
        return deepcopy(var_out)

    init_grid = Grid2D(
        delta_x=total_x / (num_pts_x - 1),
        delta_y=total_y / (num_pts_y - 1),
        u=[[u(j * total_x / num_pts_x, i * total_y / num_pts_y) for j in range(num_pts_x)] for i in range(num_pts_y)]
    )
    params = SolverParams2D(
        cfl_number=0.5,
        max_t=1.75,
        delta_t=.05
    )
    solver = Solver2D(
        initial_condition=InitialConditions2D(grid=init_grid),
        params=params,
        solver_factory=SolverFactory(method=SolverMethod.LaxFriedrichs)
    )
    return solver


def example_2d(output_path: str):
    solver = init_solver()
    solver.save(output_path + '_0')
    solver.solve(file_path=output_path)
    solver.save(output_path)


if __name__ == '__main__':
    example_2d('data/example_2d')
