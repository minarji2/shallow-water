import csv
from typing import List, Any


def create_csv(path: str, rows: List[Any]):
    with open(path, mode='w') as file:
        csv_writer = csv.writer(file, delimiter='\t')
        for row in rows:
            csv_writer.writerow(row)
