from dataclasses import dataclass
from typing import List, Optional

import numpy as np
from loguru import logger

from shallow_water.core import InitialConditions, Grid, VariablesAug1D
from shallow_water.core_2d import InitialConditions2D
from shallow_water.riemann_solver import RiemannSolver
from shallow_water.solver import SolverFactory, Solver, SolverParams
from shallow_water.utils import create_csv


@dataclass(frozen=True)
class SolverParams2D:
    cfl_number: float
    max_t: float
    delta_t: float


class Solver2D:

    def __init__(self, initial_condition: InitialConditions2D,
                 params: SolverParams2D,
                 solver_factory: SolverFactory,
                 riemann_solver: Optional[RiemannSolver] = None):
        self._riemann_solver = riemann_solver
        self._grid = initial_condition.grid
        self._params = params
        self._solver_factory = solver_factory
        self._time = 0

    def _prepare_one_solver(self, delta_step: float, u: List[VariablesAug1D], delta_t: Optional[float] = None) -> Solver:
        grid = Grid(delta_x=delta_step, u=u)
        kwargs = {}
        if self._riemann_solver is not None:
            kwargs['riemann_solver'] = self._riemann_solver
        return self._solver_factory.create(
            initial_condition=InitialConditions(grid=grid),
            params=SolverParams(cfl_number=self._params.cfl_number, max_t=delta_t),
            **kwargs
        )

    def _prepare_solvers_horizontal(self, delta_t: Optional[float] = None) -> List[Solver]:
        solvers = []
        for i in range(1, len(self._grid.u) - 1):
            u = [var2d.horizontal_aug_1d() for var2d in self._grid.u[i]]
            solvers.append(self._prepare_one_solver(delta_step=self._grid.delta_x, u=u, delta_t=delta_t))
        return solvers

    def _prepare_solvers_vertical(self, delta_t: Optional[float] = None) -> List[Solver]:
        solvers = []
        for j in range(1, len(self._grid.u[0]) - 1):
            u = [var2d_row[j].vertical_aug_1d() for var2d_row in self._grid.u]
            solvers.append(self._prepare_one_solver(delta_step=self._grid.delta_y, u=u, delta_t=delta_t))
        return solvers

    def _iteration_horizontal(self, solvers: List[Solver]):
        for k, solver in enumerate(solvers):
            i = k + 1
            u_i = solver.solve().u
            for j in range(len(self._grid.u[0])):
                self._grid.u[i][j].update_from_horizontal_aug_1d(u_i[j])

    def _iteration_vertical(self, solvers: List[Solver]):
        for k, solver in enumerate(solvers):
            j = k + 1
            u_j = solver.solve().u
            for i in range(len(self._grid.u)):
                self._grid.u[i][j].update_from_vertical_aug_1d(u_j[i])

    def _get_delta_t(self) -> float:
        hor_solvers = self._prepare_solvers_horizontal()
        ver_solvers = self._prepare_solvers_vertical()
        return np.min([s.get_delta_t() for s in hor_solvers + ver_solvers])

    def _iteration(self):
        delta_t = self._get_delta_t()
        hor_solvers = self._prepare_solvers_horizontal(delta_t)
        self._iteration_horizontal(hor_solvers)
        ver_solvers = self._prepare_solvers_vertical(delta_t)
        self._iteration_vertical(ver_solvers)
        self._time += delta_t

    def solve(self, file_path: Optional[str] = None):
        iteration = 0
        while self._time < self._params.max_t:
            self._iteration()
            iteration += 1
            logger.info('{}:\tt={}', iteration, self._time)
            if file_path is not None:
                self.save(file_path + '_' + str(iteration))

    def save(self, path: str):
        create_csv(path + '-h', [[u.h for u in u_row] for u_row in self._grid.u])
        create_csv(path + '-u', [[u.u for u in u_row] for u_row in self._grid.u])
        create_csv(path + '-v', [[u.v for u in u_row] for u_row in self._grid.u])
