import abc
from typing import Optional, List
from warnings import warn

import numpy as np

from shallow_water.core import VariablesAug1D, flux, G


class RiemannSolver(abc.ABC):

    def __init__(self):
        self._u_l: Optional[VariablesAug1D] = None
        self._u_r: Optional[VariablesAug1D] = None

    @abc.abstractmethod
    def get_flux(self, u_l: VariablesAug1D, u_r: VariablesAug1D) -> VariablesAug1D:
        pass


class RoeRiemannSolver(RiemannSolver):

    def __init__(self):
        warn('RoeRiemannSolver may be broken after the refactoring.')
        super().__init__()

    def _weighted_average(self, value_l: float, value_r: float):
        average = value_l * np.sqrt(self._u_l.h) + \
                  value_r * np.sqrt(self._u_r.h)
        average /= np.sqrt(self._u_l.h) * np.sqrt(self._u_r.h)
        return average

    def _roe_averages(self) -> VariablesAug1D:
        return VariablesAug1D(
            h=np.sqrt(self._u_l.h * self._u_r.h),
            u_h=np.sqrt(self._u_l.h * self._u_r.h) * self._weighted_average(self._u_l.u, self._u_r.u),
            psi=self._weighted_average(self._u_l.psi, self._u_r.psi),
            a=np.sqrt(0.5 * (self._u_l.a_from_h ** 2 + self._u_r.a_from_h ** 2))
        )

    @staticmethod
    def _eigenvalues(roe_avg: VariablesAug1D) -> List[float]:
        return [roe_avg.u - roe_avg.a, roe_avg.u, roe_avg.u + roe_avg.a]

    @staticmethod
    def _eigenvectors(roe_avg: VariablesAug1D) -> List[VariablesAug1D]:
        k1 = VariablesAug1D(h=1, u_h=roe_avg.u - roe_avg.a, psi=roe_avg.psi)
        k2 = VariablesAug1D(h=0, u_h=0, psi=1)
        k3 = VariablesAug1D(h=1, u_h=roe_avg.u + roe_avg.a, psi=roe_avg.psi)
        return [k1, k2, k3]

    def _wave_strengths(self, roe_avg: VariablesAug1D) -> List[float]:
        delta = self._u_r - self._u_l
        a1 = 0.5 * (delta.h - roe_avg.h * delta.u / roe_avg.a)
        a2 = roe_avg.h * delta.psi
        a3 = 0.5 * (delta.h + roe_avg.h * delta.u / roe_avg.a)
        return [a1, a2, a3]

    def _solve(self, u_l: VariablesAug1D, u_r: VariablesAug1D) -> VariablesAug1D:
        self._u_l, self._u_r = u_l, u_r
        roe_avg = self._roe_averages()
        lambdas = self._eigenvalues(roe_avg)
        alphas = self._wave_strengths(roe_avg)
        evs = self._eigenvectors(roe_avg)

        new_u = 0.5 * (u_l + u_r)
        for i in range(3):
            new_u -= 0.5 * np.sign(lambdas[i]) * alphas[i] * evs[i]
        return new_u

    def get_flux(self, u_l: VariablesAug1D, u_r: VariablesAug1D) -> VariablesAug1D:
        return flux(self._solve(u_l, u_r))


class HLLTypeRiemannSolver(RiemannSolver):

    def __init__(self, alternative_h_star: bool = True):
        super().__init__()
        self._alternative_h_star = alternative_h_star

    def _u_star_l(self, s_star: float, s_l: float) -> VariablesAug1D:
        coef = self._u_l.h * (s_l - self._u_l.u) / (s_l - s_star)
        base = VariablesAug1D(h=1, u_h=s_star, psi=self._u_l.psi)
        return coef * base

    def _u_star_r(self, s_star: float, s_r: float) -> VariablesAug1D:
        coef = self._u_r.h * (s_r - self._u_r.u) / (s_r - s_star)
        base = VariablesAug1D(h=1, u_h=s_star, psi=self._u_r.psi)
        return coef * base

    def _h_star(self) -> float:
        h_lr = self._u_l.h + self._u_r.h
        a_lr = self._u_l.a_from_h + self._u_r.a_from_h
        delta_u = self._u_r.u - self._u_l.u

        if self._alternative_h_star:
            # From (10.17)
            return 0.5 * h_lr - 0.25 * delta_u * h_lr / a_lr
        else:
            # From (10.18)
            return np.square(0.5 * a_lr - 0.25 * delta_u) / G

    def _q_l(self) -> float:
        h_star = self._h_star()
        if h_star <= self._u_l.h:
            return 1
        return np.sqrt(0.5 * h_star * (h_star + self._u_l.h) / (self._u_l.h ** 2))

    def _q_r(self) -> float:
        h_star = self._h_star()
        if h_star <= self._u_r.h:
            return 1
        return np.sqrt(0.5 * h_star * (h_star + self._u_r.h) / (self._u_r.h ** 2))

    def _s_l(self) -> float:
        return self._u_l.u - self._u_l.a_from_h * self._q_l()

    def _s_r(self) -> float:
        return self._u_r.u + self._u_r.a_from_h * self._q_r()

    @abc.abstractmethod
    def get_flux(self, u_l: VariablesAug1D, u_r: VariablesAug1D) -> VariablesAug1D:
        pass


class HLLCRiemannSolver(HLLTypeRiemannSolver):

    def __init__(self, alternative_h_star: bool = True):
        super().__init__(alternative_h_star=alternative_h_star)

    def get_flux(self, u_l: VariablesAug1D, u_r: VariablesAug1D) -> VariablesAug1D:
        self._u_l, self._u_r = u_l, u_r
        s_l, s_r = self._s_l(), self._s_r()
        s_star = s_l * self._u_r.h * (self._u_r.u - s_r) - s_r * self._u_l.h * (self._u_l.u - s_l)
        s_star /= self._u_r.h * (self._u_r.u - s_r) - self._u_l.h * (self._u_l.u - s_l)
        if 0 <= s_l:
            return flux(self._u_l)
        elif s_l <= 0 <= s_star:
            return flux(self._u_l) + s_l * (self._u_star_l(s_star, s_l) - self._u_l)
        elif s_star <= 0 <= s_r:
            return flux(self._u_r) + s_r * (self._u_star_r(s_star, s_l) - self._u_r)
        elif s_r <= 0:
            return flux(self._u_r)
        else:
            raise Exception


class HLLRiemannSolver(HLLTypeRiemannSolver):

    def __init__(self, alternative_h_star: bool = True):
        super().__init__(alternative_h_star=alternative_h_star)

    def get_flux(self, u_l: VariablesAug1D, u_r: VariablesAug1D) -> VariablesAug1D:
        self._u_l, self._u_r = u_l, u_r
        f_l, f_r = flux(u_l), flux(u_r)
        s_l, s_r = self._s_l(), self._s_r()
        if 0 <= s_l:
            return f_l
        elif s_l <= 0 <= s_r:
            f_hll = s_r * f_l - s_l * f_r + s_r * s_l * (u_r - u_l)
            f_hll = (1 / (s_r - s_l)) * f_hll
            return f_hll
        elif s_r <= 0:
            return f_r
        else:
            raise Exception
