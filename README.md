# Shallow Water Equation

This code has been developed during the course *D12ZZNR*.

## How to use

### Poetry (optional)

This project uses poetry for python dependency management. 

Install poetry:
```
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

Install all needed python dependencies (see https://python-poetry.org/docs/basic-usage/):
```
poetry install
```

### Examples

- 1D examples:
```
python shallow_water/examples.py
```

Using poetry (optional):
```
poetry run python shallow_water/examples.py
```

- 2D example:
```
python shallow_water/example_2d.py
```

Using poetry (optional):
```
poetry run python shallow_water/example_2d.py
```

### Plot

- 1D plot:
```
gnuplot plot/plot.gp
```
The figure will be saved to *results/example_1.ep*.

- 2D plot:
```
gnuplot plot/plot_2d.gp
```
The figure will be saved to *results/example_2d.ep*.
